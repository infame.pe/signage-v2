<?php


namespace App\Http\Controllers;


use App\Prediction;
use App\Product;
use Illuminate\Http\Request;

class PredictionController extends Controller
{
    public function create (Request $request)
    {
        $prediction_information = $request->all();
        $product = Product::inRandomOrder()->first();
        $prediction_information ['product_id'] = $product->id;
        $prediction = Prediction::create($prediction_information);
        return response()->json($prediction);
    }

    public function findByUser ($user_id)
    {
        $prediction = Prediction::where('user_id', $user_id)->first();
        return response()->json($prediction);
    }

    public function delete ($prediction_id)
    {
        $destroyed = Prediction::destroy($prediction_id);
        return response()->json($destroyed);
    }
}