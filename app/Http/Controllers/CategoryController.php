<?php


namespace App\Http\Controllers;


use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function list(Request $request)
    {
        $page = $request->input('page');
        $per_page = $request->input('per_page');
        $categories = Category::query();
        $categories = $categories->paginate((int)$per_page, null, null, (int)$page);
        return response()->json($categories);
    }

    public function create (Request $request)
    {
        $category_information = $request->all();
        $category = Category::create($category_information);
        return response()->json($category);
    }

    public function find($id)
    {
        $category = Category::where('id', $id)->first();
        return response()->json($category);
    }

    public function delete ($category_id)
    {
        $destroyed = Category::destroy($category_id);
        return response()->json($destroyed);
    }


}