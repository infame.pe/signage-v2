<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class JwtAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        try {

            if (! $user = \Tymon\JWTAuth\Facades\JWTAuth::parseToken()->authenticate()) {
                return response()->json($this->setUpResponse('The toke does not have a user related'));
            }

        } catch (TokenExpiredException $e) {

            return response()->json($this->setUpResponse(null, $this->refreshToken()));

        } catch (TokenInvalidException $e) {

            return response()->json($this->setUpResponse('The token is invalid'));

        } catch (JWTException $e) {

            return response()->json($this->setUpResponse('The token has several problems'));

        }

        return $next($request);
    }

    public function setUpResponse($errors = null, $token = [], $user = [])
    {
        $response = [
            'data' => [
                'token' => ($token) ? $this->setUpTokenResponse($token):$token,
                'user' => ($user) ? $this->setUpUser($user):$user
            ],
            'messages' => $errors
        ];

        return $response;
    }

    public function setUpTokenResponse($token)
    {
        $response = [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];

        return $response;
    }

    public function setUpUser($user)
    {
        $response = [
            'id' => $user->id,
            'name' => $user->name,
            'last_name' => $user->last_name,
        ];

        return $response;
    }

    public function refreshToken()
    {
        $token = JWTAuth::refresh(JWTAuth::getToken());
        return $token;
    }

}
