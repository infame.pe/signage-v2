<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function() {
    Route::post('login', 'UserController@login')->name('login');
    Route::get('logout', 'UserController@logout')->name('logout');
    Route::post('facebook/login', 'UserController@loginWithFacebook')->name('fb_login');
    Route::post('/create', 'UserController@create')->name('create')->middleware('jwt');
    Route::post('/delete/{id}', 'UserController@delete')->name('create')->middleware('jwt');
});

Route::group(['prefix' => 'products'], function() {
    Route::get('/{product_id}', 'ProductController@find')->name('find');
    Route::get('/', 'ProductController@list')->name('products');
    Route::post('/create', 'ProductController@create')->name('create')->middleware('jwt');
    Route::get('/delete/{id}', 'ProductController@delete')->name('delete')->middleware('jwt');
});

Route::group(['prefix' => 'predictions'], function() {
    Route::get('/{user_id}', 'PredictionController@findByUser')->name('find')->middleware('jwt');
    Route::post('/create', 'PredictionController@create')->name('product')->middleware('jwt');
    Route::get('/delete/{prediction_id}', 'PredictionController@delete')->name('create')->middleware('jwt');
});

Route::group(['prefix' => 'beacons'], function() {
    Route::get('/{beacon_uuid}', 'BeaconController@find')->name('beacon-find')->middleware('jwt');
    Route::get('/', 'BeaconController@list')->name('beacons')->middleware('jwt');
    Route::get('/delete/{id}', 'BeaconController@delete')->name('delete')->middleware('jwt');
    Route::post('/create', 'BeaconController@create')->name('create')->middleware('jwt');
});

Route::group(['prefix' => 'categories'], function() {
    Route::get('/', 'CategoryController@list')->name('categories');
    Route::get('/delete/{id}', 'CategoryController@delete')->name('delete')->middleware('jwt');
    Route::post('/create', 'CategoryController@create')->name('create')->middleware('jwt');
    Route::get('/{id}', 'CategoryController@find')->name('beacon-find')->middleware('jwt');
});
