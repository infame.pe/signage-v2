<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $credentials = $request->all();

        if (!auth()->attempt($credentials)) {
            return response()->json($this->setUpResponse('Access Unauthorized'), 401);
        }

        $user = auth()->user();;
        $token = JWTAuth::fromUser($user);

        return response()->json($this->setUpResponse(null, $token, $user));
    }

    public function loginWithFacebook(Request $request)
    {
        $access_token = $request->input('access_token');
        $facebook_id = $request->input('facebook_id');
        $token_user = Socialite::driver('facebook')->userFromToken($access_token);
        $facebook_user = User::Where('facebook_id', $facebook_id)->first();

        if (!$facebook_user->facebook_id) {
            return response()->json($this->setUpResponse('The user is not registered'), 401);
        }

        if ($token_user->user['id'] != $facebook_user->facebook_id) {
            return response()->json($this->setUpResponse('The access toke is incorrect'), 401);
        }

        $token = JWTAuth::fromUser($facebook_user);

        return response()->json($this->setUpResponse(null, $token, $facebook_user));
    }

    public function logout()
    {
        auth()->logout();
        return response()->json($this->setUpResponse('Successfully logged out'));
    }

    public function create (Request $request)
    {
        $user_information = $request->all();
        $user_information['password'] = Hash::make($user_information['password']);
        $user = User::create($user_information);
        return response()->json($user);
    }

    public function setUpTokenResponse($token)
    {
        $response = [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];

        return $response;
    }

    public function setUpUser($user)
    {
        $response = [
            'id' => $user->id,
            'name' => $user->name,
            'last_name' => $user->last_name,
        ];

        return $response;
    }

    public function setUpResponse($errors = null, $token = [], $user = [])
    {
        $response = [
            'data' => [
                'token' => ($token) ? $this->setUpTokenResponse($token):$token,
                'user' => ($user) ? $this->setUpUser($user):$user
            ],
            'messages' => $errors
        ];

        return $response;
    }

    public function refreshToken()
    {
        $token = JWTAuth::refresh(JWTAuth::getToken());
        return $token;
    }

    public function delete ($user_id)
    {
        $destroyed = User::destroy($user_id);
        return response()->json($destroyed);
    }

}