<?php


namespace App\Http\Controllers;


use App\Beacon;
use Illuminate\Http\Request;

class BeaconController extends Controller
{
    public function list(\Illuminate\Http\Request $request)
    {
        $page = $request->input('page');
        $per_page = $request->input('per_page');
        $beacons = Beacon::query();
        $beacons = $beacons->paginate((int)$per_page, null, null, (int)$page);
        return response()->json($beacons);
    }

    public function find($beacon_uuid)
    {
        $beacon = Beacon::where('UUID', $beacon_uuid)->first();
        return response()->json($beacon);
    }

    public function create (Request $request)
    {
        $beacon_information = $request->all();
        $beacon = Beacon::create($beacon_information);
        return response()->json($beacon);
    }

    public function delete ($beacon_id)
    {
        $destroyed = Beacon::destroy($beacon_id);
        return response()->json($destroyed);
    }

}