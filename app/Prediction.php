<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prediction extends Model
{
    protected $guarded = [];
    public function user ()
    {
        $this->belongsTo(User::class);
    }

    public function beacon ()
    {
        $this->belongsTo(Beacon::class);
    }

    public function product ()
    {
        $this->belongsTo(Product::class);
    }
}
