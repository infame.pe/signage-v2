<?php


namespace App\Http\Controllers;


use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function list(Request $request)
    {

        $filters = $request->all();
        $page = $request->input('page');
        $per_page = $request->input('per_page');

        $products = Product::with('images');

        if (array_key_exists('category', $filters)) {
            $products = $products->where('category_id', (int)$filters['category']);
        }

        $products = $products->paginate((int)$per_page, null, null, $page);

        return $products;
    }

    public function find ($product_id)
    {
        $product = Product::with('images')->find($product_id);
        return $product;
    }

    public function create (Request $request)
    {
        $product_information = $request->all();
        $product = Product::create($product_information);
        return response()->json($product);
    }

    public function delete ($product_id)
    {
        $destroyed = Product::destroy($product_id);
        return response()->json($destroyed);
    }

}